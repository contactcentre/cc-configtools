A set of tools in python that will help to validate contact centre config
Build it to a zip, copy the zip to your home dir and run it as below


# sandbox
to avoid global pip install of dependencies use a venv

python -m venv contact-centre
python setup.py develop

# run option 1
 python -m configtools --dir <config dir>

# run and update
 python -m configtools --dir <config dir> --update true

# run with external releaseNote directory
python -m configtools --dir <config dir> --releaseNotes <releaseNote dir> --update true
/home/nick.mann/development/contact-centre-admin/ContactCentreProject/releases

# wip build a dist
python3 -m pip install --upgrade pip
python3 -m pip install --upgrade build
python3 -m build

# install from whl
pip install configtools-0.0.1-py3-none-any.whl


# run option 2 (doesnt work yet) build a zip app
python -m zipapp configtools
see https://docs.python.org/3/library/zipapp.html
cp configtools.pyz ~

python -m ~/configtools.pyz --dir <config directory>

problem is the relase note reading needs special handling
other problem is the relative imports don't work grrr
# tests
 python -m unittest discover ./test

 some tests fail when they are run all together - file handles not closed i think

 can run them in vscode if you add the python extension

 # Adding new commands
 see https://gammatelecom.atlassian.net/browse/CON-5933 for the work to do
 to add a new cmd use syntax.py as a template
## TODO simplify the cmd names




