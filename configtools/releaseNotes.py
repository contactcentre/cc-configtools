from .releaseNote import ReleaseNote
import os
import sys
from pathlib import Path

class ReleaseNotes:
    def __init__(self, releaseNoteDir):
        self._releaseNotes = []
        #WIP - problems trying to load the release not when we are zipped up
        # resourceDir = Path(Path(__file__).parent, "release_notes")
        # # if not os.path.exists(resourceDir):
        # #     sys.exit()
        # # print ("releaseNotes in ", resourceDir)
        # with zipfile.ZipFile(os.path.dirname(__file__)) as z:
        #     print(z.namelist())
        #     resources = zipfile.Path(z, "release_notes")
        #     for resource in resources.iterdir():
        #         print(resource)
        
        # for root, _, files in os.walk(resourceDir):
        #     for file in files:
        #         print(file)
        #         if file.endswith(".json"):
        #             self._releaseNotes.append(ReleaseNote(os.path.join(root, file)))
        resourceDir = Path(Path(__file__).parent, "test", "release_notes")
        
        if (releaseNoteDir != None):
            resourceDir = Path(releaseNoteDir)
        
        if not resourceDir.is_dir or not os.path.exists(resourceDir):
            sys.exit("invalid release note dir {0}".format(resourceDir))
        for root, _, files in os.walk(resourceDir):
            for file in files:
                if file.endswith(".json"):
                    self._releaseNotes.append(ReleaseNote(os.path.join(root, file)))


    def latest(self):
        latest = None
        for note in self._releaseNotes:
            if (latest is None) or note.version() > latest.version():
                latest = note
        print("using release note {0}".format(latest.version().base_version))
        return latest

    def version(self, version):
        for note in self._releaseNotes:
            if note.version() == version:
                return note