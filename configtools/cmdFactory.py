from .syntaxCmd import ValidateSyntaxCmd
from .containerVersionsCmd import ValidateContainerVersionsCmd

class CmdFactory:
    cmds = {}

    def __init__(self, context):
        self.context = context
        self.addCmd(ValidateSyntaxCmd(context))
        self.addCmd(ValidateContainerVersionsCmd(context))

    def addCmd(self, cmd):
        self.cmds[cmd.name()] = cmd

    def cmd(self, name):
        return self.cmds[name]
    
    def all(self):
        for cmd in self.cmds.values():
            cmd.execute()

