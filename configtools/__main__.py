#!/usr/bin/env python3
import argparse

from  .cmdContext import CmdContext
from  .cmdFactory import CmdFactory

parser = argparse.ArgumentParser()
parser.add_argument('--dir', help='a directory that contains contact centre configuration')
parser.add_argument('--releaseNotes', help='a directory that contains contact centre releaseNotes')
parser.add_argument('--cmd', help='the cmd to run', default="all")
parser.add_argument('--update', help='rewrite the versions', default="false")
parser.add_argument('--testSystem', help='ignore latest and staging etc', default="false")
args = parser.parse_args()

context = CmdContext(args.dir, update = args.update, testSystem=args.testSystem, releaseNotes=args.releaseNotes)
cmdFactory = CmdFactory(context)
if args.cmd == "all":
    cmdFactory.all()
else:
    cmdFactory.cmd(args.cmd).execute()
