import os
from .cmd import Cmd
from packaging import version as semver

class ValidateContainerVersionsCmd(Cmd):
    def __init__(self, context):
        self.context = context
        super().__init__("validateContainerVersions")

    def execute(self):
        # get the versions
        containerVersionsByComposeFile = self.context.composeFiles.getContainerVersionsByFile()

        # get the family
        # family = self.context.composeFiles.getContainerVersions()

        # get the expected versions for the family
        expectedContainerVersions = self.context.releaseNotes.latest()

        for compose, composeContainerVersions in containerVersionsByComposeFile.items():
            self._validate(expectedContainerVersions, compose, composeContainerVersions)

    def _validate(self, expectedContainerVersions, compose, composeContainerVersions):
        #loop and check expectations
        failCount = 0
        warningCount = 0
        relpath = os.path.relpath(compose.dir, self.context.root)

        for containerVersion in composeContainerVersions:
            container = containerVersion["container"]
            version = containerVersion["version"]
            expectedContainer = expectedContainerVersions.container(container)
            expectedVersion = semver.parse("none") if expectedContainer == None else semver.parse(expectedContainer["version"])  

            if expectedVersion.base_version == "none":
                print("{0} not found in release note {1}".format(container, expectedContainerVersions.version()))
                warningCount += 1
                continue
            if version.base_version == "latest":
                print("WARNING: {0} at 'latest' should be {1}".format(container, expectedVersion.base_version))
                compose.updateService(containerVersion, expectedVersion)
                if self.context.testSystem == True:
                    warningCount += 1
                else:
                    failCount += 1
                continue
            if version.base_version == "staging":
                print("WARNING: {0} at 'staging' should be {1}".format(container, expectedVersion.base_version))
                compose.updateService(containerVersion, expectedVersion)
                if self.context.testSystem == True:
                    warningCount += 1
                else:
                    failCount += 1
                continue
            if version != expectedVersion:
                print("{0}:{1} should be {2}".format(container, version.base_version, expectedVersion.base_version))
                compose.updateService(containerVersion, expectedVersion)
                failCount += 1
        if failCount > 0:
            print("{0} errors found in {1}".format(failCount, relpath))
            if self.context.update:
                print("UPDATING {0}".format(relpath))
                compose.dump()
                print("")
        if warningCount > 0:
            print("{0} warnings found in {1}".format(warningCount, relpath))
            print("")
           

