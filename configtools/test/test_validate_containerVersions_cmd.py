import unittest
import os
import sys
from configtools.cmdFactory import CmdFactory
from configtools.cmdContext import CmdContext

class TestValidateContainerVersions(unittest.TestCase):
    def test_success(self):
        cmdFactory = self.cmdFactory(os.path.join(os.path.dirname(__file__), "resources/validSet"))
        cmdFactory.cmd("validateContainerVersions").execute()

    def test_bad_version(self):
        cmdFactory = self.cmdFactory(os.path.join(os.path.dirname(__file__), "resources/badVersion"))
        try:
            cmdFactory.cmd("validateContainerVersions").execute()
            raise Exception("should have failed with bad version")
        except:
            print("fine")

    def cmdFactory(self, dir):
        return CmdFactory(CmdContext(dir))