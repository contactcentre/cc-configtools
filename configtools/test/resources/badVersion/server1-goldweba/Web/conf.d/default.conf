# vi: ft=nginx sw=4 sts=4 ts=8 et

# Provides uri variable without the leading /internal/<first_path_segment>/
map $uri $uri_without_internal_and_segment {
    ~/internal/([^/]*)/(.*)  $2;
    default  $uri;
}

server {
    listen 80;
    listen 443 ssl;
    server_name localhost;

    ssl_certificate /etc/nginx/ssl/nginx.crt;
    ssl_certificate_key /etc/nginx/ssl/nginx.key;

    include conf.d/include.d/ssl.server_opts.conf;

    root /var/www/html/ContactCentreWeb;

    add_header Cache-Control 'no-cache';

    location ^~ /ws {
        proxy_pass http://ws;
        include conf.d/include.d/generic-ws.location_opts.conf;
    }

    location ^~ /webrtc {
        proxy_pass http://wswebrtc;
        include conf.d/include.d/generic-ws.location_opts.conf;
    }


    # Rest API proxying
    location ^~ /api/v1 {
        proxy_pass http://api;
    }

    # Docs go to WSP for validation
    location ^~ /doc {
        proxy_pass http://api;
    }

    # Elastic Stack (Kibana) goes to WSP for validation
    location ^~ /es {
        proxy_pass http://api;
    }

    # Prometheus & Grafana go to WSP for validation
    location ^~ /prometheus {
        proxy_pass http://api;
    }
    location ^~ /grafana {
        proxy_pass http://api;
    }

    # After validating for security,
    # WSP redirects here to /internal/PATH by an X-Accel-Redirect
    location ^~ /internal {
        internal;
        location ^~ /internal/doc {
            root /var/www/html/;
            # First look under /var/www/html/Docs-overrides/ then under /var/www/html/Docs
            try_files /Docs-overrides/$uri_without_internal_and_segment /Docs/$uri_without_internal_and_segment =404;
        }
        location ^~ /internal/es {
            proxy_pass http://kibana/es;
        }
        location ^~ /internal/prometheus {
            proxy_pass http://prometheus/prometheus;
        }
        location ^~ /internal/grafana {
            proxy_pass http://grafana/grafana;
        }
    }

    # Map studio
    location ^~ /atlas {
        proxy_pass http://atlas;
    }

    # Redirect map studio signin to contact centre login
    location ^~ /atlas/signin {
        return 302 /login;
    }

    location ^~ /assets {
        # if assets are missing we want a 404 - i.e. don't use the try_files below
        location ^~ /assets/i18n/overrides {
            # but it's OK if overrides don't exist
            try_files $uri =204;
        }
    }

    location ^~ /resource/webchat/client/downloads/ {
        proxy_pass http://cc-attachment-server/;
        add_header 'Access-Control-Allow-Origin' "$http_origin";
        add_header 'Access-Control-Allow-Methods' 'GET, OPTIONS';
        add_header 'Vary' 'Origin';
    }

    location ^~ /resource/webchat/client {
        proxy_pass http://cc-xmpp-webchat-loader/;
        add_header 'Access-Control-Allow-Origin' "$http_origin";
        add_header 'Access-Control-Allow-Methods' 'GET';
        add_header 'Vary' 'Origin';
    }

    location ^~ /resource/webchat/bosh {
        proxy_pass http://cc-xmpp-webchat-bosh/;
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-For $remote_addr;
        proxy_buffering off;
        tcp_nodelay on;
    }

    # Request javascript file for zendesk app
    location ^~ /resource/zendesk {
        proxy_pass http://zendesk/;
    }

    location / {
        try_files $uri $uri/ /index.html;
    }
}

