USE ContactCentre;

TRUNCATE TABLE 'Endpoints';

INSERT INTO 'Endpoints' ('Entity', 'Instance', 'URL', 'Active', 'Site', 'Server', 'Status') VALUES ('AgentManager',     0, 'http://golda:12040', 1, 0, 0, NULL);
INSERT INTO 'Endpoints' ('Entity', 'Instance', 'URL', 'Active', 'Site', 'Server', 'Status') VALUES ('AgentManager',     1, 'http://goldb:12040', 1, 1, 1, NULL);
INSERT INTO 'Endpoints' ('Entity', 'Instance', 'URL', 'Active', 'Site', 'Server', 'Status') VALUES ('Broker',           0, 'http://10.16.106.10',      1, 0, 0, NULL);
INSERT INTO 'Endpoints' ('Entity', 'Instance', 'URL', 'Active', 'Site', 'Server', 'Status') VALUES ('Broker',           1, 'http://10.16.106.11',      1, 1, 1, NULL);
INSERT INTO 'Endpoints' ('Entity', 'Instance', 'URL', 'Active', 'Site', 'Server', 'Status') VALUES ('Cache',           0, 'http://golda:2232',      1, 0, 0, NULL);
INSERT INTO 'Endpoints' ('Entity', 'Instance', 'URL', 'Active', 'Site', 'Server', 'Status') VALUES ('Cache',           1, 'http://goldb:2232',      1, 1, 1, NULL);
INSERT INTO 'Endpoints' ('Entity', 'Instance', 'URL', 'Active', 'Site', 'Server', 'Status') VALUES ('CallFlowService',  0, 'http://golda:12020', 1, 0, 0, NULL);
INSERT INTO 'Endpoints' ('Entity', 'Instance', 'URL', 'Active', 'Site', 'Server', 'Status') VALUES ('CallFlowService',  1, 'http://goldb:12020', 1, 1, 1, NULL);
INSERT INTO 'Endpoints' ('Entity', 'Instance', 'URL', 'Active', 'Site', 'Server', 'Status') VALUES ('HttpAdapter',      0, 'http://10.16.106.10:8888', 1, 0, 0, NULL);
INSERT INTO 'Endpoints' ('Entity', 'Instance', 'URL', 'Active', 'Site', 'Server', 'Status') VALUES ('HttpAdapter',      1, 'http://10.16.106.11:8888', 1, 1, 1, NULL);
INSERT INTO 'Endpoints' ('Entity', 'Instance', 'URL', 'Active', 'Site', 'Server', 'Status') VALUES ('LoggingService',   0, 'http://golda:12060', 1, 0, 0, NULL);
INSERT INTO 'Endpoints' ('Entity', 'Instance', 'URL', 'Active', 'Site', 'Server', 'Status') VALUES ('LoggingService',   1, 'http://goldb:12060', 1, 1, 1, NULL);
INSERT INTO 'Endpoints' ('Entity', 'Instance', 'URL', 'Active', 'Site', 'Server', 'Status') VALUES ('WebSocketProxy',   0, 'http://golda:12000', 1, 0, 0, NULL);
INSERT INTO 'Endpoints' ('Entity', 'Instance', 'URL', 'Active', 'Site', 'Server', 'Status') VALUES ('WebSocketProxy',   1, 'http://goldb:12000', 1, 1, 1, NULL);
INSERT INTO 'Endpoints' ('Entity', 'Instance', 'URL', 'Active', 'Site', 'Server', 'Status') VALUES ('WebhookDelivery',  0, 'http://golda:12003', 1, 0, 0, NULL);
INSERT INTO 'Endpoints' ('Entity', 'Instance', 'URL', 'Active', 'Site', 'Server', 'Status') VALUES ('WebhookDelivery',  1, 'http://goldb:12003', 1, 1, 1, NULL);
INSERT INTO 'Endpoints' ('Entity', 'Instance', 'URL', 'Active', 'Site', 'Server', 'Status') VALUES ('BwXsiInterface',   0, 'http://golda:12010', 1, 0, 0, NULL);
INSERT INTO 'Endpoints' ('Entity', 'Instance', 'URL', 'Active', 'Site', 'Server', 'Status') VALUES ('BwXsiInterface',   1, 'http://goldb:12010', 1, 1, 1, NULL);
/* There is only one instance of the XSI Simulator */
INSERT INTO 'Endpoints' ('Entity', 'Instance', 'URL', 'Active', 'Site', 'Server', 'Status') VALUES ('BwXsiSimulator',   0, 'http://golda:12030', 1, 0, 0, NULL);
/* EmailManager is not dual redundant so only have a single instance */
INSERT INTO 'Endpoints' ('Entity', 'Instance', 'URL', 'Active', 'Site', 'Server', 'Status') VALUES ('EmailManager',     1, 'http://golda:12080', 1, 0, 0, NULL);
/* XMPP Chat Components */
INSERT INTO 'Endpoints' ('Entity', 'Instance', 'URL', 'Active', 'Site', 'Server', 'Status') VALUES ('WorkItemManager',  0, 'http://golda:3600',  1, 0, 0, NULL);
INSERT INTO 'Endpoints' ('Entity', 'Instance', 'URL', 'Active', 'Site', 'Server', 'Status') VALUES ('WorkItemManager',  1, 'http://goldb:3600',  1, 1, 1, NULL);
INSERT INTO 'Endpoints' ('Entity', 'Instance', 'URL', 'Active', 'Site', 'Server', 'Status') VALUES ('XmppChatService',  0, 'http://golda:12070', 1, 0, 0, NULL);
INSERT INTO 'Endpoints' ('Entity', 'Instance', 'URL', 'Active', 'Site', 'Server', 'Status') VALUES ('XmppChatService',  1, 'http://goldb:12070', 1, 1, 1, NULL);
INSERT INTO 'Endpoints' ('Entity', 'Instance', 'URL', 'Active', 'Site', 'Server', 'Status') VALUES ('FileTxService',    0, 'http://golda:12110', 1, 0, 0, NULL);
INSERT INTO 'Endpoints' ('Entity', 'Instance', 'URL', 'Active', 'Site', 'Server', 'Status') VALUES ('FileTxService',    1, 'http://goldb:12110', 1, 1, 1, NULL);
INSERT INTO 'Endpoints' ('Entity', 'Instance', 'URL', 'Active', 'Site', 'Server', 'Status') VALUES ('AttachmentService',    0, 'http://golda:12073', 1, 0, 0, NULL);
INSERT INTO 'Endpoints' ('Entity', 'Instance', 'URL', 'Active', 'Site', 'Server', 'Status') VALUES ('AttachmentService',    1, 'http://goldb:12073', 1, 1, 1, NULL);
