# vi: ft=nginx sw=4 sts=4 ts=8 et

#######
# Proxy buffer tuning
# based on:
# https://www.getpagespeed.com/server-setup/nginx/tuning-proxy_buffer_size-in-nginx

# Buffers default to 4|8k based on memory page size (with busy buffers being
# double). We need more than 4k for our token, and better to be explicit so it
# doesn't work at one customer and break at another.

# This is the max size of all upstream headers.
# If it's too small nginx returns a 502.
# Our token currently maxes out around 4k, so this gives us some headroom.
# It's also the hard-coded max size for request headers that can be received by
# node, so we're getting 502s from nginx because of this, there's no point
# upping it, as chances are we'd start breaking node upstream anyway.
proxy_buffer_size 8k;

# These are for the rest of the response. If the response doesn't fit in these
# buffers that nginx buffers to disk (potentially killing performance).
# Use 4k buffers as that's the linux page size.
# The production main.js of CC Web is ~12M, so lets allow up to 24M for a bit
# of headroom. For dev purposes this will likely need to be higher though.
proxy_buffers 6144 4K;

# Recomendation from the link is $proxy_buffer_size + 2 * $proxy_buffers.size
proxy_busy_buffers_size 16k;


#######
# Extra proxy headers for all proxied requests

# Create some maps to only set proxy headers if not currently set, similar in
# function to $proxy_add_x_forwarded_for
map $http_x_real_ip $proxy_add_x_real_ip {
    ''      $remote_addr;
    default $http_x_real_ip;
}
map $http_x_forwarded_host $proxy_add_x_forwarded_host {
    ''      $host;
    default $http_x_forwarded_host;
}
map $http_x_forwarded_proto $proxy_add_x_forwarded_proto {
    ''      $scheme;
    default $http_x_forwarded_proto;
}

# X-Forwarded-For is incremental, so always just add to it.
proxy_set_header X-Forwarded-For        $proxy_add_x_forwarded_for;
# Gateway servers need to unconditionaly set the rest, so clients don't spoof.
#proxy_set_header X-Real-IP              $remote_addr;
#proxy_set_header X-Forwarded-Host       $host;
#proxy_set_header X-Forwarded-Proto      $scheme;
# Internal servers should prefer the values from the gateway server.
proxy_set_header X-Real-IP              $proxy_add_x_real_ip;
proxy_set_header X-Forwarded-Host       $proxy_add_x_forwarded_host;
proxy_set_header X-Forwarded-Proto      $proxy_add_x_forwarded_proto;

