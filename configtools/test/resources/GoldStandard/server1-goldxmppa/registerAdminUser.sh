#!/bin/sh
#
# You only need to run this once in order to create the admin@localhost account with the supplied password.
#
docker exec -it xmpp-server /home/ejabberd/bin/ejabberdctl register admin localhost telsis >> /dev/null
#
# Once the admin user has been created, you can log in at http://localhost:5280/admin/ Don't forget to include
# the localhost part of the username. Once you are logged in, you can change the password.
