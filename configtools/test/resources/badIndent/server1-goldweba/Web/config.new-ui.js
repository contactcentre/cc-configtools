var TopLevelConfig = {

    // Set non-null to make the banner to show a product name rather than default translated 'Contact Centre'
    customProductName: 'Gold System (New Interface)',

    // These may be set to relative URLs (starting with a /)
    restServerUrl: '/api/v1',
    webSocketUrl: '/ws',
    webRtcUrl: '/webrtc',
    mapStudioUrl: '/atlas',
    internalCRMUrl: '/api/v1/crm',

    // How long a session is kept in local storage before it is cleared.
    // This is to handle the case you leave a tab for a long time and refresh
    // the page. At this point it will clear an old token if its greater than the timeout.
    // This is required because we check for the presence of a token in lots of places in the code.
    userSessionStorageTimeoutMins: 15,

    // Whether to use 'sendLog' or 'runApex' for SalesForce call logging
    useRunApexCallLogging: false, // If false uses 'sendLog' API

    // whether to allow single section selection or multiple in agent scripts
    singleSectionScriptSupport: true,

    // Whether to include Call Handling in the omnichannel agent portal
    omnichannelCallHandling: true,

    // Whether or not to show the confirmation dialog box when any Queue recording parameters are changed
    showQueueRecordUpdateConfirm: false,

    // Path of the resources used for webchat client application
    WebchatResources: 'https://gold.eng.telsis.com/resource/webchat/client/',

    // Deprecated:
    WebchatConverseCSS: 'https://gold.eng.telsis.com/resource/webchat/client/converse.css',
    WebchatConverseJS: 'https://gold.eng.telsis.com/resource/webchat/client/converse.js',
    WebchatConverseEmojiJS: 'https://gold.eng.telsis.com/resource/webchat/client/emojis.js',

    // Name of the web component used for webchat client application
    WebchatComponent: 'tcc',

    // Name of the XMPP domain used for webchat client application
    WebchatXmppDomain: 'xmpp.gold.eng.telsis.com',

    // Name of the BOSH service URL used for webchat client application
    WebchatBOSHServiceURL: 'https://gold.eng.telsis.com/resource/webchat/bosh/bosh',

    StatsEventRedistributionEnabled: true
};
