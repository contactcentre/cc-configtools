import unittest
import os
import sys
from configtools.cmdFactory import CmdFactory
from configtools.cmdContext import CmdContext

class TestAll(unittest.TestCase):
    def test_success(self):
        cmdFactory = self.cmdFactory(os.path.join(os.path.dirname(__file__), "resources/validSet"))
        cmdFactory.all()

    def test_bad_version(self):
        cmdFactory = self.cmdFactory(os.path.join(os.path.dirname(__file__), "resources/badVersion"))
        try:
            cmdFactory.all()
            raise Exception("should have failed with bad version")
        except:
            print("fine")

    def cmdFactory(self, dir):
        return CmdFactory(CmdContext(dir))