import unittest
import os
import sys
from composeFiles import ComposeFiles

class TestComposeFiles(unittest.TestCase):
    def test_success(self):
        composeFiles = ComposeFiles(os.path.join(os.path.dirname(__file__), "resources/validSet"))
        composeFiles.validateSyntax()

    def test_bad_indent(self):
        composeFiles = ComposeFiles(os.path.join(os.path.dirname(__file__), "resources/badIndent"))
        try:
            composeFiles.validateSyntax()
            raise Exception("should have failed with bad indent")
        except:
            print("fine")

    def test_nbsp(self):
        composeFiles = ComposeFiles(os.path.join(os.path.dirname(__file__), "resources/nbsp"))
        try:
            composeFiles.validateSyntax()
            raise Exception("should have failed with bad nbsp")
        except:
            print("fine")