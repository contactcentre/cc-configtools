import unittest
import os
import sys
from configtools.cmdFactory import CmdFactory
from configtools.cmdContext import CmdContext

class TestValidateSyntax(unittest.TestCase):
    def test_success(self):
        cmdFactory = self.cmdFactory(os.path.join(os.path.dirname(__file__), "resources/validSet"))
        cmdFactory.cmd("validateSyntax").execute()

    def test_bad_indent(self):
        cmdFactory = self.cmdFactory(os.path.join(os.path.dirname(__file__), "resources/badIndent"))
        try:
            cmdFactory.cmd("validateSyntax").execute()
            raise Exception("should have failed with bad indent")
        except:
            print("fine")

    def test_nbsp(self):
        cmdFactory = self.cmdFactory(os.path.join(os.path.dirname(__file__), "resources/nbsp"))
        try:
            cmdFactory.cmd("validateSyntax").execute()
            raise Exception("should have failed with bad nbsp")
        except:
            print("fine")

    def cmdFactory(self, dir):
        context = CmdContext(dir)
        return CmdFactory(context)