from .composeFiles import ComposeFiles
from .releaseNotes import ReleaseNotes
import os

class CmdContext:

    def __init__(self, configDir, update = "false", testSystem = "false", releaseNotes = None):
        self.composeFiles = ComposeFiles(configDir)
        self.releaseNotes = ReleaseNotes(releaseNotes)
        self.root = configDir
        self.update = update == "true"
        self.testSystem = testSystem == "true"
        self.allNonComposeFiles = []
        self.__collectFiles(configDir)

    def __collectFiles(self, dir):
        for root, _, files in os.walk(dir):
            for file in files:
                if file != "docker-compose.yml":
                    self.allNonComposeFiles.append(os.path.join(root, file))