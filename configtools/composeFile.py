import subprocess
import os.path
import io
from packaging import version as semver
from ruamel.yaml import YAML

class ComposeFile:
    yaml = YAML()
    yaml.preserve_quotes = True
    yaml.sequence_dash_offset = 2
    yaml.width = 4096

    def __init__(self, file):
        self._containers = None
        self.composeFile = file
        self.dir = os.path.dirname(file)
    
    def parse(self):
        if self._containers != None:
            return
        # Read YAML file
        with open(self.composeFile, 'r') as stream:
            self.composeYaml = self.yaml.load(stream)
        self._containers = []
        for service in self.composeYaml["services"].values():
            self._containers.append(self.extractContainer(service))

    def updateService(self, containerVersion, newVersion):
        for service in self.composeYaml["services"].values():
            if containerVersion["container"] in service["image"] and containerVersion["version"].base_version in service["image"]:
                replacedImage = service["image"].replace(containerVersion["version"].base_version, newVersion.base_version)
                service["image"] = replacedImage

    def dump(self):
        with open(self.composeFile, 'w') as file:
            self.yaml.dump(self.composeYaml, file)

    def extractContainer(self, service):
        image = service["image"].split(":")
        container = image[0].replace("docker.eng.tcs.local/", "")
        version = semver.parse(image[1]) if len(image) == 2 else semver.parse("latest")
        return {
            "version": version,
            "container": container
        }

    def validateSyntax(self):
        # print("validating", self.composeFile)
        dcValidate = subprocess.run(
            ["docker-compose", "config"],
            cwd = self.dir,
            stdout=subprocess.DEVNULL,
            stderr=subprocess.STDOUT)
        if (dcValidate.returncode != 0):
            if dcValidate.stdout:
                print(dcValidate.stdout.decode('utf-8'))
            print("invalid compose {0}".format(self.dir))
        dcValidate.check_returncode()

    #def family(self):
        # monitoring, core, web, devops

    def containers(self):
        self.parse()
        return self._containers
