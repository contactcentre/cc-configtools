from .cmd import Cmd
import subprocess
import os
from ruamel.yaml import YAML

class ValidateSyntaxCmd(Cmd):
    yaml = YAML()
    yaml.allow_duplicate_keys = True

    def __init__(self, context):
        self.context = context
        super().__init__("validateSyntax")

    def execute(self):
        errCount = 0;
        for file in self.context.allNonComposeFiles:
            relPath = os.path.relpath(file, self.context.root)
            if file.endswith(".yml"):
                try:
                    with open(file, 'r') as stream:
                        self.yaml.load(stream)
                except Exception as e:
                    errCount += 1
                    print("invalid yaml {0}: {1}".format(relPath, e))

        for file in self.context.allNonComposeFiles:
            with open(file) as f:
                relPath = os.path.relpath(file, self.context.root)
                try:
                    if '\xa0' in f.read():
                        print("invalid whitespace in {0}".format(relPath))
                        errCount += 1
                except:
                    if (not file.endswith(".p12")):
                        print("warning couldn't check whitespace: {0}".format(relPath))
        if errCount > 0:
            raise Exception("failed syntax check")
        self.context.composeFiles.validateSyntax()
        print("passed syntax and whitespace check")
        print("")