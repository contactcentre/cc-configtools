import os
import sys
import os.path
from pathlib import Path
from .composeFile import ComposeFile

class ComposeFiles:

    def __init__(self, configDir):
        self._composeFiles = []
        self.dir = Path(configDir)
        if self.dir.is_dir and os.path.exists(self.dir):
            print("")
            print ("****** Welcome to Contact Centre Config Validation tool")
            print ("scanning config directory ", self.dir)
        else:
            sys.exit("invalid config dir {}".format(self.dir))
        self.__collectFiles(self.dir)

    def __collectFiles(self, dir):
        for root, _, files in os.walk(dir):
            for file in files:
                if file.endswith("docker-compose.yml"):
                    self._composeFiles.append(ComposeFile(os.path.join(root, file)))

    def validateSyntax(self):
        for composeFile in self._composeFiles:
            composeFile.validateSyntax()

    def getContainerVersionsByFile(self):
        containers = {}
        for composeFile in self._composeFiles:
            containers[composeFile] = composeFile.containers()
        return containers
