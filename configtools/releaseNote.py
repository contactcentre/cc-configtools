import os.path
from pathlib import Path
import json
from packaging import version

class ReleaseNote:

    def __init__(self, note):
        self._parsedReleaseNote = []
        self._containersByName = {}
        self._version = None
        basename = os.path.basename(note)
        self._version = version.parse(basename.replace(".json", ""))
        # parse note
        self._parsedReleaseNote = json.loads(Path(note).read_text())
        for component in self._parsedReleaseNote:
            if component["type"] == "Collection":
                for subComponent in component["contains"]:
                    self._containersByName[subComponent["name"]] = subComponent
            else:
                self._containersByName[component["name"]] = component

    def version(self):
        return self._version

    def container(self, name):
        if not(name in self._containersByName.keys()):
            return None
        return self._containersByName[name]
