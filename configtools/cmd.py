class Cmd:
    _name = ""
    def __init__(self, name):
        self._name = name
        # print("instantiate command {}".format(name))

    def execute(self, context):
        print("running command {}".format(self.name))

    def name(self):
        return self._name