from setuptools import find_packages, setup

setup(name='configtools',
      version='0.0.1-dev',
      description='contact centre config tools',
      install_requires=['packaging', 'ruamel.yaml'],
      packages=find_packages(where="configtools"),
      zip_safe=False)